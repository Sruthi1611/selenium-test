package dailychallengeweek4;

import java.util.Scanner;

public class LessButNotContainDigit {

	public static void main(String[] args) {
		// Less than a input number but not contain input digit


		Scanner sc =new Scanner(System.in);
		System.out.println("Enter one digit: ");
		String digit = sc.next();
		System.out.println("Enter one number: ");
		int number = sc.nextInt();

		while(number>0) {
			if(Integer.toString(number).contains(digit)) {

				number--;
			}else {

				System.out.println("Immediate lesser number of the input number not contains digit " +digit+" is "+number);	
				break;
			}
		}

	}


}


