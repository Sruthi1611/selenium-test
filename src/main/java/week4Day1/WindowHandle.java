package week4Day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandle {

	public static void main(String[] args) {
		// TO switch to different windows and perform actions
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.findElementByLinkText("AGENT LOGIN").click();
		WebElement activeElement = driver.switchTo().activeElement();
		System.out.println(activeElement.getAttribute("class"));
		driver.findElementByLinkText("Contact Us").click();
		Set<String> windowHandles = driver.getWindowHandles();
		System.out.println("Number of open windows: " + windowHandles.size());
		System.out.println(windowHandles);
		List<String> list = new ArrayList<String>();
		list.addAll(windowHandles);
		driver.switchTo().window(list.get(1));
		System.out.println("Title of the second window: " +driver.getTitle());
		System.out.println("Url of current window: " +driver.getCurrentUrl());
		String text = driver.findElementByXPath("/html/body/div/div[2]/b/p[2]").getText();
		System.out.println(text);
		
		driver.close();
		driver.switchTo().window(list.get(0));
		driver.findElementById("usernameId").sendKeys("Sruthi");
		
		driver.close();
		
		
		
		

	}

}
