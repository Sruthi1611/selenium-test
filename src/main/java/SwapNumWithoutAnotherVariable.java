import java.util.Scanner;

public class SwapNumWithoutAnotherVariable {

	public static void main(String[] args) {
		// To swap two number without using third variable

		Scanner sc = new Scanner(System.in);
		System.out.println(" Enter two numbers to swap:");
		int a = sc.nextInt();
		int b = sc.nextInt();
		System.out.println("Before swappting: a= " +a + " and b= " +b);
		a=a+b;
		b=a-b;
		a=a-b;
		System.out.println("After swappting: a= " +a + " and b= " +b);

		sc.close();
	}

}
