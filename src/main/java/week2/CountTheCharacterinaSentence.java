package week2;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import javax.print.attribute.HashAttributeSet;

public class CountTheCharacterinaSentence {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner get = new Scanner(System.in);
		System.out.print("Enter the String: ");
		String a = get.next();
		int b = a.length();
		Map<Character, Integer> c = new LinkedHashMap<Character, Integer>();

		for (int i = 0; i < b; i++) 
		{
			char d = a.charAt(i);
			if (c.containsKey(d)) {
				c.put(d, c.get(d)+1);
			}else
			{
				c.put(d, 1);
			}

		}
		System.out.println(c);
		get.close();
	}

}


