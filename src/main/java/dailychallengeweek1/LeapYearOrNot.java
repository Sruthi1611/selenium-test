package dailychallengeweek1;

import java.util.Scanner;

public class LeapYearOrNot {

	public static void main(String[] args) {
		// TO find a number is a leap year or not

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a year to find whether its leap year or not ");
		int year = sc.nextInt();
		if(((year%100 == 0) && (year%400 == 0)) || ( (year%100 != 0) && (year%4 ==0))) {

			System.out.println(year + " is a Leap Year");
		}
		else {
			System.out.println(year + " is not a Leap year");
		}sc.close();
	}



}
