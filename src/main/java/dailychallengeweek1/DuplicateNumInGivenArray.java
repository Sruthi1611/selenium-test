package dailychallengeweek1;

import java.util.HashSet;
import java.util.Set;

public class DuplicateNumInGivenArray {

	
	public static void main(String[] args) {

		// To print duplicate numbers in an given array

		int a[] ={13,65,15,67,88,65,13,99,67,13,65,87,13};
		Set<Integer> dup = new HashSet<Integer>();
		Set<Integer> uniqNum = new HashSet<Integer>();


		for (int num : a) {

			if (dup.add(num) == false) {

				uniqNum.add(num);

			}
		}
		System.out.println("Duplicate Element in an array is : "
				+ uniqNum);



	}

}
