package dailychallengeweek1;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class ArraysAscendingandDescending {

	public static void main(String[] args) {

			Scanner ip = new Scanner(System.in);
			System.out.println("Enter the length of the array: ");
			int m = ip.nextInt();
			Integer[] a = new Integer[m];
			System.out.println("Enter " +m + " input array values: ");		
			for (int i=0;i<m;i++)
			{
				a[i] = ip.nextInt();
			}
			
			Arrays.sort(a);
			System.out.println("Ascending Order: " + Arrays.toString(a));
			Arrays.sort(a, Collections.reverseOrder());
			System.out.println("Descending Order: " + Arrays.toString(a));
			ip.close();

		}


}