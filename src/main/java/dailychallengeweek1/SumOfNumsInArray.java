package dailychallengeweek1;

import java.util.Scanner;

public class SumOfNumsInArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the length of array: ");
		int length = sc.nextInt();
		int m[] = new int[length];
		
		System.out.println("Enter the array values: ");
		for(int i=0;i<length;i++) {
			m[i] = sc.nextInt();
		}
		int sum= 0;
			for (int j : m) {
				sum=sum+j;

			}
			System.out.println(sum);
		}

	}

