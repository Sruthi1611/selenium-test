package dailychallengeweek1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DropDown {

	public static void main(String[] args) {
		// TO select last option from Drop down 

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		driver.get("http://www.leafground.com/pages/Dropdown.html");

		Select obj = new Select(driver.findElementById("dropdown1"));
		List<WebElement> options = obj.getOptions();
		int size1 = options.size();
		obj.selectByIndex(size1-1);

		Select obj2 = new Select(driver.findElementByName("dropdown2"));
		obj2.selectByVisibleText("Selenium");

		Select obj3 = new Select(driver.findElementById("dropdown3"));
		obj3.selectByValue("2");

		Select obj4 = new Select(driver.findElementByClassName("dropdown"));
		List<WebElement> allopt = obj4.getOptions();
		System.out.println("No.of.options in the drop down: " + allopt.size());
		for (WebElement eachopt : allopt) {
			System.out.println(eachopt.getText());

		}
		obj4.selectByValue("3");

		driver.findElementByXPath("//div[@id=\"contentblock\"]/section/div[5]/select").sendKeys("Selenium");




	}

}
