package dailychallengeweek1;

import java.util.Scanner;

public class PrimeNumberstillN {

	public static void main(String[] args) {
		// To print prime numbers

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number: ");
		int b = sc.nextInt();
		for(int j=2;j<=b;j++) {
			boolean flag = true;
			for(int i=2;i<j;i++)	{

				if(j%i==0) {

					flag = false;
					break;

				}
			}

			if( flag) {
				System.out.println(j);
			}
		}
		sc.close();


	}
}
