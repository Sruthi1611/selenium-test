package dailychallengeweek1;

import java.util.Scanner;

public class PrintWordUsingLogicalOperator {

	public static void main(String[] args) {
		// TO print word as per condition

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter two numbers: ");
		int a = sc.nextInt();
		int b = sc.nextInt();

		for (int i = a; i<b ;i++)
		{
			if(i%3== 0 && i%5 ==0) {
				System.out.print(" FIZZBUZZ ");

			}
			else if(i%3 == 0 ) {
				System.out.print(" FIZZ ");
			}
			else if(i%5 == 0) {
				System.out.print(" BUZZ ");
			}
			else
			{
				System.out.print(" " + i );

			}
		}sc.close();

	}

}
