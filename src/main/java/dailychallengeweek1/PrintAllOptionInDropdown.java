package dailychallengeweek1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class PrintAllOptionInDropdown {

	public static void main(String[] args) {
		// TO print all options  in a dropdown
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.get("https://www.irctc.co.in/");
		
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Sign up").click();
		/*driver.findElementById("userRegistrationForm:userName").sendKeys("Sruthi");
		driver.findElementById("userRegistrationForm:password").sendKeys("Angelin1611");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Angelin1611");
		
		Select obj = new Select(driver.findElementById("userRegistrationForm:securityQ"));
		obj.selectByValue("6");
		
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("YamahaFz");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Sruthi");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Angelin");
		driver.findElementById("userRegistrationForm:gender:0").click();
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		
		Select obj1 = new Select(driver.findElementById("userRegistrationForm:dobDay"));
		obj1.selectByValue("08");
		
		Select obj2 = new Select(driver.findElementById("userRegistrationForm:dobMonth"));
		obj2.selectByVisibleText("MAY");
		
		Select obj3 = new Select(driver.findElementById("userRegistrationForm:dateOfBirth"));
		obj3.selectByValue("1994");
		
		Select obj4 = new Select(driver.findElementById("userRegistrationForm:occupation"));
		obj4.selectByVisibleText("SelfEmployed");
		
		*/Select obj5 = new Select(driver.findElementById("userRegistrationForm:countries"));
		obj5.selectByValue("94");
		List<WebElement> options = obj5.getOptions();
		for (WebElement eachctry : options) {
			String list = eachctry.getText();
			System.out.println(list);
		}

	}

}
