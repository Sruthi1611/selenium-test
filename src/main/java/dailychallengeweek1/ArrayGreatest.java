package dailychallengeweek1;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class ArrayGreatest {

	public static void main(String[] args) {
		// TO find the First greatest and third greatest

		Scanner ip = new Scanner(System.in);
		System.out.println("Enter the length of the array: ");
		int m = ip.nextInt();
		Integer[] a = new Integer[m];
		System.out.println("Enter " +m + " input values to find first largest and third largest: ");		
		for (int i=0;i<m;i++)
		{
			a[i] = ip.nextInt();
		}
		Arrays.sort(a, Collections.reverseOrder());
		//Arrays.toString(a);
		System.out.println("The Second largest is " + a[1]);
		ip.close();

	}

}
