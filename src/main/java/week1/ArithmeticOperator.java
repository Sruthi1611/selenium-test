package week1;

public class ArithmeticOperator {

	public static void main(String[] args) {
		// all arithmetic operators
		
		int n1 = 5;
		int n2 = 8;
		
		System.out.println("Addition of two numbers is " +(n1+n2));
		System.out.println("Subraction of two numbers is " +(n1-n2));
		System.out.println("Multiplication of two numbers is " + (n1*n2));
		System.out.println("Quotient of two numbers is " + (n1/n2));
		System.out.println("Remainder of two numbers is " +(n1%n2));
		System.out.println(n1 + " power " + n2 +" is " +(Math.pow(n1, n2)) );
		if(n1>n2)
		{
			System.out.println(n1 + " is greatest");
			System.out.println(n2+ " is smallest");
		}else if(n1<n2){
			System.out.println(n2 + " is greatest");
			System.out.println(n1 + " is smallest");
			
		}else {
			System.out.println(n1+ " and " + n2 + "are equal");
		}
		

	}

}
