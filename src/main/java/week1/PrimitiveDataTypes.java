package week1;

public class PrimitiveDataTypes {

	public static void main(String[] args) {
		// To list and print all primitive data types
		
		boolean flag = true;
		
		//only one character, blank is also a character, 16 bit
		char a= ' ';
		// 1byte = 8 bit,-128 to 127 max value
		byte b = 123;
		//short=16bit,-32768 to 32767 max value
		short c = 12345;
		//int = 32 bit
		int d = 2234567;
		//long = 64 bit,
		long e = 12344566777987l;
		//double = 32 bit
		float f =122655339446899765f;
		//float = 64 bit
		double g = 12345677899004567899d;
		
		
		System.out.println("char " + a + '\n' + "byte " +b+ '\n' + "short " + c + '\n' + "int " + d + '\n'+ "long " + e + '\n'
				+"float " + f + '\n'+ "double " + g);

	}

}
