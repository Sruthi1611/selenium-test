package week5.day1;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReportprg {

	public static ExtentTest test;
	public static ExtentReports extent;
	public static String testCaseName, testCaseDescription, author, category,filenamedesc;

	@BeforeSuite(groups="common")
	public void startResult() {
		// TO create a report using Extent Report tool
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result1.html");
		html.setAppendExisting(true);

		extent=new ExtentReports();
		// editable file
		extent.attachReporter(html);
	}

	@BeforeMethod(groups="common")
	public void startTestCase() {
		test = extent.createTest(testCaseName, testCaseDescription);
		test.assignAuthor(author);
		test.assignCategory(category);
	}

	public void reportStep(String description, String status) {

		if(status.equalsIgnoreCase("pass")){
			test.pass(description);
		}
		else if(status.equalsIgnoreCase("fail")){
			test.fail(description);
		}
	}

	@AfterSuite(groups="common")
	public void stopResult() {
		extent.flush();
	}
}
