package week5.day1;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import FrameWorkBasics.SeMethods;
import week6.ReadExcelApachePoiOoxml;

public class ProjectMethods extends SeMethods {

	@DataProvider(name = "inputs")
	public Object[][] getdata() throws IOException {
		// TODO Auto-generated method stub
		Object[][] getdata = ReadExcelApachePoiOoxml.getdata(filenamedesc);
		        return getdata;
	}
	
	@BeforeMethod(groups="common")
	@Parameters({"browser","url","username","password"})
	public  void login(String browserName,String url,String userName,String pswd) {
		// TODO Auto-generated method stub
		
		startApp(browserName, url);
		 WebElement usernm = locateElement("id", "username");
		 type(usernm, userName);
		 WebElement passwd = locateElement("id","password");
		 type(passwd, pswd);
		 WebElement login = locateElement("class","decorativeSubmit");
		 click(login);
		 WebElement crm = locateElement("linktext", "CRM/SFA");
		 click(crm);
	}
	@AfterMethod(groups="common")
	public void close() {
		closeBrowser();
	}
	
	
	

}
