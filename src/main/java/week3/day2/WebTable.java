package week3.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://erail.in/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("SBC",Keys.TAB);
		boolean selected = driver.findElementById("chkSelectDateOnly").isSelected();
		{
			if(selected) {
				driver.findElementById("chkSelectDateOnly").click();
			}
		}
		
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
	    List<WebElement> allrows = table.findElements(By.tagName("tr"));
	    for (WebElement eachrow : allrows) {
	    	List<WebElement> eachcol = eachrow.findElements(By.tagName("td"));
	     System.out.println(eachcol.get(1).getText());
		}
//	    List<WebElement> tr = col2.findElements(By.tagName("tr"));
//	    String text = tr.get(0).getText();
//	    System.out.println(text);
//	    
	    
		
		
	

	}

}
