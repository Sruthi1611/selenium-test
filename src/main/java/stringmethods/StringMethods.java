package stringmethods;

import java.util.Scanner;

public class StringMethods {

	public static void main(String[] args) {
		// using all the string methods
		Scanner ip = new Scanner(System.in);
	    System.out.println("Enter a string:");
	    String s = ip.nextLine();
	    
	  //to print length
		System.out.println("Length of a string is" +s.length());
		
		//to print charArray
		char c[] = s.toCharArray();
		for(char d: c) {
		System.out.println("Char Array of a string" + d);
		}
		
		//to  print charAt
		System.out.println("CharAt at  position 2 is" + s.charAt(2));
		
		//to print lower case
		System.out.println("Lower case of a string is" + s.toLowerCase());
		
		//to print upper case
		System.out.println("Upper case of a string is " + s.toUpperCase() );
		
		//to find equals
		
		String r = "Sruthi";
		System.out.println("Compare two  strings" + r.equals(s));
		
		System.out.println("Compare two strings ignore case" + r.equalsIgnoreCase(s));
		
		System.out.println("Substring" + s.substring(0, 5));
		System.out.println("Index" + s.indexOf("ut"));
		System.out.println("last index of" +s.lastIndexOf(s));
		
		System.out.println("trim" +s.trim());
		
		String a[] = s.split("u");
		for(String b: a)
		{
		System.out.println("split" + b);
		}
		
		System.out.println("Replace" + s.replace('i', 'm'));
		System.out.println("Concat" + s.concat(r));
		System.out.println("Starts with" + s.startsWith("Sr"));
		System.out.println("Ends with" + s.endsWith("lin"));
		
	    
        
	}

}
