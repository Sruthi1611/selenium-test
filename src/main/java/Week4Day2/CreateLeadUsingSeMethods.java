package Week4Day2;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import week5.day1.ProjectMethods;
import week6.ReadExcelApachePoiOoxml;

public class CreateLeadUsingSeMethods extends ProjectMethods  {


	@BeforeClass(groups="common")
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testCaseDescription = "create lead";
		author = "Sruthi";
		category = "smoke";
		filenamedesc = "CreateLead";
	}
	//@Test(invocationCount=2,invocationTimeOut=30000)
	@Test(groups="smoke",dataProvider="inputs")
	public void createLead(String cName, String fName, String lname, String areCd,String phnNum) {
		// TO implement methods from super class and create lead

		//login();
		WebElement crtlead = locateElement("linktext", "Create Lead");
		click(crtlead);
		WebElement cmpny = locateElement("id", "createLeadForm_companyName");
		type(cmpny, cName);
		WebElement frstnm = locateElement("id","createLeadForm_firstName");
		type(frstnm, fName);
		WebElement lstnm = locateElement("id", "createLeadForm_lastName");
		type(lstnm, lname);
		WebElement pharea = locateElement("id", "createLeadForm_primaryPhoneAreaCode");
		type(pharea, areCd);
		WebElement psnum = locateElement("id", "createLeadForm_primaryPhoneNumber");
		type(psnum, phnNum);

		WebElement crct = locateElement("xpath", "//input[@value = 'Create Lead']");
		click(crct);

		//closeBrowser();






	}


}


