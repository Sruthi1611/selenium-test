package Week4Day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import week5.day1.ProjectMethods;

public class EditLeadUsingSeMethods extends ProjectMethods {

	@BeforeClass(groups="common")
	public void setData() {
		testCaseName = "TC003_EditLead";
		testCaseDescription = "Edit lead";
		author = "Sruthi";
		category = "smoke";
		filenamedesc = "EditLead";

	}
	//@Test(dependsOnMethods ="Week4Day2.CreateLeadUsingSeMethods.createLead",enabled = false)
	@Test(groups ="sanity",dependsOnGroups="smoke", dataProvider="inputs")
	public void editLead(String fName, String cName) throws InterruptedException {
		// Edit lead using SeMethods..SeMethods present in FrameWorkBasics package




		//login();
		WebElement lead = locateElement("linktext", "Leads");
		click(lead);
		WebElement findl = locateElement("linktext", "Find Leads");
		click(findl);
		WebElement id = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(id, fName);
		WebElement btlead = locateElement("xpath", " //button[text()='Find Leads']");
		click(btlead);
		WebElement lid = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		Thread.sleep(3000);
		click(lid);
		WebElement edit = locateElement("linktext", "Edit");
		click(edit);
		WebElement cmpny = locateElement("id", "updateLeadForm_companyName");
		cmpny.clear();
		type(cmpny, cName);
		WebElement update = locateElement("xpath", "//input[@value='Update']");
		click(update);
		WebElement updatd = locateElement("id", "viewLead_companyName_sp");
		String text = getText(updatd);
		System.out.println(" Updated company name is" + text);








	}






}


