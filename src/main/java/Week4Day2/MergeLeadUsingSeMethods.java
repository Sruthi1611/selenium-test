package Week4Day2;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import FrameWorkBasics.SeMethods;

public class MergeLeadUsingSeMethods extends SeMethods {
	@Test
	public void mergeLed() throws InterruptedException {
		// To merge lead using Semethods

		startApp("chrome", "http://leaftaps.com/opentaps/control/main");
		WebElement usernm = locateElement("id", "username");
		type(usernm, "DemoSalesManager");
		WebElement passwd = locateElement("id","password");
		type(passwd, "crmsfa");
		WebElement login = locateElement("class","decorativeSubmit");
		click(login);
		WebElement crm = locateElement("LinkText", "CRM/SFA");
		click(crm);
		WebElement lead = locateElement("LinkText", "Leads");
		click(lead);
		WebElement findl = locateElement("LinkText", "Merge Leads");
		click(findl);
		WebElement img1 = locateElement("xpath", "//input[@id='partyIdFrom']/following-sibling::a/img");
		click(img1);
		switchToWindow(1);
		WebElement loc1 = locateElement("xpath", "//input[@name='id']");
		type(loc1, "10593");
		WebElement floc1 = locateElement("xpath", "//button[text()='Find Leads']");
		click(floc1);

		WebElement slc1 = locateElement("LinkText", "10593");
		clickWithNoSnap(slc1);
		switchToWindow(0);
		WebElement img2 = locateElement("xpath", "//input[@id='partyIdTo']/following-sibling::a/img");
		click(img2);
		switchToWindow(1);
		WebElement loc2 = locateElement("xpath", "//input[@name='id']");
		type(loc2, "10472");
		WebElement floc2 = locateElement("xpath", "//button[text()='Find Leads']");
		click(floc2);
		WebElement slc2 = locateElement("LinkText", "10472");
		clickWithNoSnap(slc2);
		switchToWindow(0);
		takeSnap();
		WebElement merge = locateElement("LinkText", "Merge");
		clickWithNoSnap(merge);
		acceptAlert();

		WebElement eleflead = locateElement("LinkText", "Find Leads");
		click(eleflead);
		WebElement loc3 = locateElement("xpath", "//input[@name='id']");
		type(loc3, "10593");

		WebElement fndld = locateElement("xpath", "//button[text()='Find Leads']");
		click(fndld);

		WebElement errmsg = locateElement("class", "x-paging-info");
		String errtxt = getText(errmsg);
		System.out.println("Verified the Error message: " + errtxt);









	}

}
