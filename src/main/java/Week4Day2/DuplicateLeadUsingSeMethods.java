package Week4Day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import week5.day1.ProjectMethods;

public class DuplicateLeadUsingSeMethods extends ProjectMethods {
	@BeforeClass
	public void setData() {
		testCaseName = "TC002_DuplicateLead";
		testCaseDescription = "duplicate lead";
		author = "Sruthi";
		category = "smoke";
	}
	
    @Test
	public  void duplicateLead() throws InterruptedException {
		// TO duplicate a lead using SeMethods
		
         //login();
		 WebElement lead = locateElement("LinkText", "Leads");
		 click(lead);
		 WebElement findl = locateElement("LinkText", "Find Leads");
		 click(findl);
		 WebElement email = locateElement("LinkText", "Email");
		 click(email);
		 WebElement address = locateElement("name", "emailAddress");
		 type(address, "sruthiangelin@gmail.com");
		 WebElement btlead = locateElement("xpath", " //button[text()='Find Leads']");
		 click(btlead);
		 WebElement lid = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		 Thread.sleep(3000);
		 click(lid);
		 WebElement dupl = locateElement("LinkText", "Duplicate Lead");
		 click(dupl);
		 boolean texttitle = verifyTitle("Duplicate Lead | opentaps CRM");
		 System.out.println("Verified the title Duplicate Lead " +texttitle);
		 /**System.out.println(text);
		 String text2 = locateElement("id", "createLeadForm_primaryEmail").getText();
		 **/
		 WebElement submit = locateElement("class", "smallSubmit");
		 click(submit);
		
		

	}

}
