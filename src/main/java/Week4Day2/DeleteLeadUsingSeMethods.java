package Week4Day2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import week5.day1.ProjectMethods;

public class DeleteLeadUsingSeMethods  extends ProjectMethods{

	@BeforeClass(groups="common")
	public void setData() {
		testCaseName = "TC003_DeleteLead";
		testCaseDescription = " Delete lead";
		author = "Sruthi";
		category = "smoke";
		filenamedesc = "DeleteLead";

	}


	//@Test(dependsOnMethods= "Week4Day2.EditLeadUsingSeMethods.editLead")
	@Test(groups="regression",dependsOnGroups="sanity",dataProvider = "inputs")
	public void DelLead(String phnCd,String phnNum) {
		//			login();
		WebElement elelead = locateElement("linktext", "Leads");
		click(elelead);
		WebElement eleflead = locateElement("linktext", "Find Leads");
		click(eleflead);
		WebElement eleph = locateElement("linktext", "Phone");
		click(eleph);
		WebElement pharcd = locateElement("name", "phoneAreaCode");
		type(pharcd, phnCd);
		WebElement phnum = locateElement("name", "phoneNumber");
		type(phnum, phnNum);
		WebElement fndld = locateElement("xpath", "//button[text()='Find Leads']");
		click(fndld);

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a")));

		WebElement fleadid = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		String text = getText(fleadid);
		click(fleadid);
		WebElement del = locateElement("linktext", "Delete");
		click(del);
		WebElement eleflead1 = locateElement("linktext", "Find Leads");
		click(eleflead1);
		WebElement leadid = locateElement("name", "id");
		type(leadid, text);
		WebElement fndld1 = locateElement("xpath", "//button[text()='Find Leads']");
		click(fndld1);
		WebElement errmsg = locateElement("class", "x-paging-info");
		String text2 = getText(errmsg);
		System.out.println("Message is: " + text2);
	}

	
}

